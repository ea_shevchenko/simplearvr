﻿using UnityEngine;
using System.Collections;

namespace EventSystem.AREventSystem{
public class TouchHandler : MonoBehaviour {


   public string[] animationTitles;
   private GameObject player = null;
   private	Animator playerAnimation;
   

	void Start () {
	player = GameObject.FindGameObjectWithTag ("Player");
	playerAnimation = player.GetComponent<Animator>();	
	}
	
	void Update () {
			foreach (Touch touch in Input.touches) {
				if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled){
					playRandomAnimation ();		
			}
     	 }
     }

		public void playRandomAnimation(){
			string state = animationTitles[Random.Range(0, animationTitles.Length - 1)];
			playerAnimation.Play(state);
		}

		public void PlayBonusAnimation(){
			string animationName = string.Format("Bonus{0}", Random.Range((int) 1, (int) 3));
			playerAnimation.SetTrigger (animationName);

		}
  }
}
