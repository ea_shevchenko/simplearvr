﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace EventSystem
{
   
    public abstract class BaseMessagingManager : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField]
        protected bool EnableDebug = false;
#endif

        #region Logic Process Function
       
        public BaseMessagingManager()
        {
            CreateAllStorage();
        }
			    
        protected virtual void OnDestroy()
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("OnDestroy");
#endif
            DestoryAllStorage();
        }
			      
        protected abstract void CreateAllStorage();
		    
        protected abstract void DestoryAllStorage();
        #endregion

        #region Message Function
       
        public void CreateStorage<D, I>() where D : IMessageDomain where I : IMessageListener
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("CreateStorage<" + typeof(D).ToString() + "," + typeof(I).ToString() + ">()");
#endif

            HandlerStorage<D, I>.Handlers = new List<I>();
        }
			      
        public void DestroyStorage<D, I>() where D : IMessageDomain where I : IMessageListener
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("DestroyStorage<" + typeof(D).ToString() + "," + typeof(I).ToString() + ">()");
#endif

            HandlerStorage<D, I>.Handlers = null;
        }
			      
        public void Add<D, I>(I handler) where D : IMessageDomain where I : IMessageListener
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("Add<" + typeof(D).ToString() + "," + typeof(I).ToString() + ">()");
#endif

            Assert.IsNotNull<List<I>>(HandlerStorage<D, I>.Handlers, "Handler storage not created yet!");

            if (HandlerStorage<D, I>.Handlers != null)
            {
                HandlerStorage<D, I>.Handlers.Add(handler);
            }
        }
			       
        public void Remove<D, I>(I handler) where D : IMessageDomain where I : IMessageListener
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("Remove<" + typeof(D).ToString() + "," + typeof(I).ToString() + ">()");
#endif

            if (HandlerStorage<D, I>.Handlers != null)
            {
                HandlerStorage<D, I>.Handlers.Remove(handler);

                if (HandlerStorage<D, I>.Handlers.Count == 0)
                {
                    HandlerStorage<D, I>.Handlers = null;
                }
            }
        }
			      
        public void Broadcast<D, I>(Action<I> action) where D : IMessageDomain where I : IMessageListener
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("Broadcast<" + typeof(D).ToString() + "," + typeof(I).ToString() + ">()");
#endif

            if (HandlerStorage<D, I>.Handlers != null)
            {
                var handlersCount = HandlerStorage<D, I>.Handlers.Count;
                for (var i = 0; i < handlersCount; i++)
                {
                    action(HandlerStorage<D, I>.Handlers[i]);
                }
            }
        }
			       
        public IEnumerator BroadcastAsync<D, I>(Action<I> action) where D : IMessageDomain where I : IMessageListener
        {
#if UNITY_EDITOR
            if (EnableDebug) Debug.Log("BroadcastAsync<" + typeof(D).ToString() + "," + typeof(I).ToString() + ">()");
#endif
            yield return null;

            Broadcast<D, I>(action);
        }
        #endregion
    }
}
