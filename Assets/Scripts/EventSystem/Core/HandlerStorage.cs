﻿using System.Collections.Generic;

namespace EventSystem
{
   
    internal static class HandlerStorage<D, I> where D : IMessageDomain where I : IMessageListener
    {
        private static List<I> m_Handlers;

        
        public static bool IsReady
        {
            get { return (m_Handlers != null); }
        }
			    
        public static List<I> Handlers
        {
            get { return m_Handlers; }
            set { m_Handlers = value; }
        }
    }
}
