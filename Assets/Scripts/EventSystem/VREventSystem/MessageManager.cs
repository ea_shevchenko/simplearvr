﻿namespace EventSystem.VREventSystem
{
    public class MessageManager : BaseMessagingManager
    {
        protected override void CreateAllStorage()
        {
            CreateStorage<GlobalMessage, Message>();
        }

        protected override void DestoryAllStorage()
        {
            DestroyStorage<GlobalMessage, Message>();
        }
    }
}