﻿namespace EventSystem.VREventSystem
{
    public interface Message : IMessageListener
    {
        void VRMessage(string message);
    }
}