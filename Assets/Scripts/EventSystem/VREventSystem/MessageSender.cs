﻿using System.Diagnostics;
using UnityEngine;

namespace EventSystem.VREventSystem
{
    public class MessageSender : MonoBehaviour
    {
        [SerializeField]
        MessageManager manager;
		public bool debug;

		// broadcast some event message from active item
        public void SendSync()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
			manager.Broadcast<GlobalMessage, Message>((handler) => handler.VRMessage(gameObject.transform.name));
            watch.Stop();
			if (debug) {
				UnityEngine.Debug.Log("Time: " + watch.ElapsedMilliseconds);
			}           		
        }			    
    }
}