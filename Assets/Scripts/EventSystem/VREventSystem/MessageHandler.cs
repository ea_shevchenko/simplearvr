﻿using UnityEngine;
using System.Collections;

namespace EventSystem.VREventSystem
{
    public class MessageHandler : MonoBehaviour, Message
    {
        [SerializeField]
        MessageManager manager;

		//boolean variable to trigger of cat moving
		private bool canMove = true;
		//name of active item
		private string objName = "";
		private NavMeshAgent agent = null;
		private GameObject targetObj = null;
		//fixed time of starting cat event
		public float timeToHandle = 0f;
		public float catSpeed = 0f;
		public float catAcceleration = 0.1f;
		//debug flag
		public bool debug;

		public bool finish = false;
	
		void Start(){
			agent = GetComponent<NavMeshAgent> ();
		}
			
		void FixedUpdate(){
			if (debug) {
				Debug.Log (canMove);
			}
			canMove = finish ? true : false;
			if (canMove) {
				targetObj = GameObject.Find (objName);
				move ();
			}
		}

		//move cat to active item/ so if cat move - he always must look to target
		private void move(){
			if (objName.Length > 0 && targetObj != null) {
				setPhysicsParamsOfNavmeshAgent ();
				agent.destination = targetObj.transform.position;
				var rotation = Quaternion.LookRotation(targetObj.transform.position - transform.position);
				transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);
			}
		}

		//set some physics parametrs of cat navmesh agent if cat move
		private void setPhysicsParamsOfNavmeshAgent(){
			agent.speed = catSpeed;
			agent.acceleration = catAcceleration;
		}

        void OnEnable(){
            manager.Add<GlobalMessage, Message>(this);
        }

        void OnDisable()
        {
            manager.Remove<GlobalMessage, Message>(this);
        }

        public void VRMessage(string message) {
			if (debug) {
				Debug.Log(message);
			}
			objName = message;
			canMove = true;
			StartCoroutine (moveToTarget ());
        }

		IEnumerator moveToTarget() {
			yield return new WaitForSeconds (timeToHandle);
			finish = true;
		}
    }
}
