﻿using UnityEngine;
using System.Collections;
using Interactions;
using EventSystem.VREventSystem;

public class CatAnimationHandler : MonoBehaviour {

	private NavMeshAgent agent = null;
	private Animator catAnimator = null;
	private float velocity = 0;
	private GameObject itemObj;
	public bool canEat = false;

	void Start () {
		agent = gameObject.GetComponent<NavMeshAgent> ();
		catAnimator = gameObject.GetComponent<Animator> ();
		itemObj = GameObject.Find("Item");
		revertPhysicsParamsOfNavmeshAgent();
	}
	
	// Update is called once per frame
	void Update () {
		handleAnimationState ();
	}

	//physics parametrs of cat navmesh agent equals zero 
	private void revertPhysicsParamsOfNavmeshAgent(){
		agent.velocity = Vector3.zero;
		agent.speed = 0;
    	agent.acceleration = 0f;
	}

	private void handleAnimationState(){
		
		if (canEat == true) {
			catAnimator.SetBool ("eat", true);
			revertPhysicsParamsOfNavmeshAgent();
			agent.Stop ();
		} else {
			catAnimator.SetBool ("eat", false);
			agent.Resume ();
		}
		catAnimator.SetFloat ("speed", agent.speed);
		} 		
}
