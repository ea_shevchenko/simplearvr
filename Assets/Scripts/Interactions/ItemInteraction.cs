﻿using UnityEngine;
using System.Collections;
using EventSystem;
using EventSystem.VREventSystem;

namespace Interactions{
public class ItemInteraction : MonoBehaviour {

	public MessageHandler msgHandler;
	public float animationTime;
    private GameObject catObj;
	[SerializeField]
    CatAnimationHandler handler;

	void Start () {
	catObj = GameObject.FindGameObjectWithTag ("Player");			
 }

	void Update () {
	
	 }

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			handler.canEat = true;			
			StartCoroutine (interaction());
	}
}

		IEnumerator interaction(){			
			yield return new WaitForSeconds(animationTime);
			handler.canEat = false;
			Destroy (gameObject);

		}
    }
}
