# README #
#Simple augmented / virtual reality reality project#
Application structure:
*Main menu Scene
*VR mode
*VR Scene
*Reset (Restart scene)
*Main Menu (open “Main menu” scene)
*AR Mode
*AR Scene


Augmented reality mode
Technical requirements:
*Create and insert into database («Vuforia» project profile) imageTag (маркер).
*Camera on marker = cube appear.
*Tap on screen = random rotation cube.

Virtual reality mode
Technical requirements:
*Viewport camera react on player look.
*Cursor in the middle of the screen («Raycast»).
*Active objects (character and interaction objects = 3)
*Highlight active object when cursor looks at it. 
*When 1.5 sec have past – character moves to active object and destroy it.
Visual requirements:
*Quadruped creature.
*Full setup (skin + rig)
*Hi res texture
Animations
*Idle
*Walk
*Eat
*Misc.
Environment (for VR)
*Creature
*Food
*Ground
*Elements of environment